package net.kivitechnologies.BrainWork.Examination.Training21;

import java.util.ArrayList;

public class Matrix {
    private ArrayList<ArrayList<Object>> matrix;
    
    public Matrix() {
        matrix = new ArrayList<>();
    }
    
    public void set(int x, int y, double value) {
        while(matrix.size() <= x) 
            matrix.add(new ArrayList<Object>());
        
        ArrayList<Object> column = matrix.get(x); 
        
        while(column.size() <= y) 
            column.add(null);
        
        column.set(y, value);
    }
    
    public double get(int x, int y) {
        return (Double)matrix.get(x).get(y);
    }
    
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        ArrayList<Object> column;
        for(int x = 0; x < matrix.size(); x++) {
            column = matrix.get(x);
            for(int y = 0; y < column.size(); y++) {
                if(column.get(y) != null)
                    builder.append(String.format("%6.3f", column.get(y)));
                else
                    builder.append("      ");
                
                builder.append(" ");
            }
            builder.append("\n");
        }
        
        return builder.toString();
    }
}
