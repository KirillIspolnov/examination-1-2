package net.kivitechnologies.BrainWork.Examination.Training21;

public class Demo {

    public static void main(String[] args) {
        Matrix matrix = new Matrix();
        matrix.set(0, 0, 3);
        matrix.set(1, 1, 10);
        matrix.set(2, 2, 15);
        matrix.set(0, 1, 7);
        matrix.set(0, 2, -3);
        
        System.out.println(matrix);
    }

}
