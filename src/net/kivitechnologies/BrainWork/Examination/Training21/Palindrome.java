package net.kivitechnologies.BrainWork.Examination.Training21;

import java.util.Scanner;

public class Palindrome {
    private static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        String phrase = readPhrase();
        if(isPalyndrome(phrase)) {
            System.out.printf("Ух ты! Это %s - палиндром!", getCorrectName(phrase));
        } else {
            System.out.printf("Сожалею, но это обычное %s", getCorrectName(phrase));
        }
    }

    private static String readPhrase() {
        System.out.println("Введите фразу для проверки на полиндромность: ");
        String phrase = scanner.nextLine().toLowerCase();
        
        while(phrase.isEmpty() || !phrase.matches("[a-zа-я0-9.,;:?!]+")) {
            System.out.println("Кажется, в фразе присутствует недопустимый символ. Введите фразу снова: ");
            phrase = scanner.nextLine().toLowerCase();
        }
        
        return phrase;
    }
    
    private static String reverse(String initial) {
        StringBuffer buffer = new StringBuffer(initial);
        buffer = buffer.reverse();
        return buffer.toString();
    }
    
    private static String reverse(String initial) {
        StringBuilder builder = new StringBuilder();
        for(int i = initial.length() - 1; i > - 1; i--) {
            builder.append(initial.charAt(i));
        } 
        return builder.toString();
    } 
    
    private static boolean isPalyndrome(String phrase) {
        phrase = phrase.replaceAll("[^a-zа-я0-9]", "");
        return reverse(phrase).equals(phrase);
    }
    
    private static String getCorrectName(String phrase) {
        return phrase.contains(" ") ? "предложение" : "слово";
    }
}
