package net.kivitechnologies.BrainWork.Examination.Training19;

/**
 * Created by g16oit18 on 19.12.2017.
 */
public class TargetOvalsAndRings {
    public static void main(String[] args) {
        double[] ovals = new double[5];

        for(int i = 0; i < ovals.length; i++) {
            ovals[i] = Math.pow(i + 1, 2) * Math.PI;
            System.out.printf("Площадь %d круга = %.3f%n", i + 1, ovals[i]);
        }

        for(int i = 1; i < ovals.length; i++) {
            System.out.printf("Площадь %d кольца = %.3f%n", i, ovals[i] - ovals[i - 1]);
        }
    }
}
