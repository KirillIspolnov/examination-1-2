package net.kivitechnologies.BrainWork.Examination.Training19;

/**
 * Created by g16oit18 on 19.12.2017.
 */
public class BankAccount {
    /**
     * Баланс
     */
    private double balance;
    /**
     * Процентная ставка
     */
    private double percentage;

    /**
     * Инициализирует счет заданными балансом (сумма рублей и копеек) и процентной ставкой
     *
     * @param roubles рубли
     * @param pennys  копейки
     * @param percentage процентная ставка
     */
    public BankAccount(int roubles, int pennys, double percentage) {
        this.balance = roubles + pennys / 100;
        this.percentage = percentage;
    }

    /**
     * Инициализирует счет заданными балансом и процентной ставкой
     *
     * @param roubles рубли
     * @param percentage процентная ставка
     */
    public BankAccount(int roubles, double percentage) {
        this(roubles, 0, percentage);
    }

    /**
     * Инициализирует счет значениями по умолчанию - нулевым счетом и процентной ставкой в 0,01 процента
     */
    public BankAccount() {
        this(0, 0.01);
    }

    /**
     * Обновляет баланс счета по проществии года
     */
    public void update() {
        this.balance *= (percentage / 100 + 1);
    }

    public String toString() {
        return String.format("На счету с процентной ставкой %.2f процентов годовых лежит %.2f рублей", percentage, balance);
    }
}
