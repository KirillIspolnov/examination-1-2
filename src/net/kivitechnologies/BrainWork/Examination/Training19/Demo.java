package net.kivitechnologies.BrainWork.Examination.Training19;

/**
 * Created by g16oit18 on 19.12.2017.
 */
public class Demo {
    public static void main(String[] args) {
        BankAccount account = new BankAccount(15000, 73, 7.3);
        BankAccount account1 = new BankAccount(7893, 3.2);
        BankAccount account2 = new BankAccount();

        System.out.printf("%s%n%s%n%s%n", account, account1, account2);
        account.update();
        account1.update();
        account2.update();
        System.out.printf("%s%n%s%n%s", account, account1, account2);
    }
}
